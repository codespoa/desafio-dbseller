<?php

/*
Página responsável por:
Listagem de áreas.
*/

use DAO\Area;

$areas = Area::getInstance()->order('id', 'desc')->getAll();
?>
<div class="container">
  <table class="table">
    <thead>
      <tr>
        <th scope="col">ID</th>
        <th scope="col">Nome</th>
        <th scope="col">Editar</th>
        <th scope="col">Apagar</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach($areas as $area) : ?>
        <tr>
          <th scope="row"><?=$area->id?></th>
          <td><?=$area->descricao?></td>
          <td><a class="btn btn-warning" role="button" href="?path=editar-area&id=<?=$area->id?>"><i class="fas fa-edit"></i></a></td>
          <td><a class="btn btn-danger" role="button" href="?path=apagar-area&id=<?=$area->id?>"><i class="fas fa-trash"></i></a></td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
  <a class="btn btn-success btn-lg" role="button" href="?path=editar-area">Cadastrar</a>
</div>
